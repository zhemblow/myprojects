# ZHMBLMyProjects

##On this page you can learn about the projects, the creation of which I was directly involved


###Stade de France (November 2016 - February 2017)
https://itunes.apple.com/by/developer/stade-de-france/id819305610

###Matmut ATLANTIQUE (November 2016 - February 2017)
https://itunes.apple.com/by/developer/netco-sports/id294408179

###Flow Sports (August 2016 - October 2016)
https://itunes.apple.com/by/developer/cable-wireless-holdings-inc./id874354221

###SBS World News (June 2016 - August 2016)
https://itunes.apple.com/by/developer/special-broadcasting-service/id410148194

###SFR Sport (May 2016 - June 2016)
https://itunes.apple.com/app/id984491525

###World Endurance Championship (February 2016 - April 2016)
The FIA World Endurance Championship: A global competition played out in nine different countries on four continent. The cream of top motoring manufacturers – Aston Martin, Audi, Ferrari, Ford, Porsche and Toyota – as well as a host of legendary motorsport names, competing in the ultimate test of man and machine to become world endurance champion. The application includes a in-app purchases.
https://itunes.apple.com/by/developer/fia-world-endurance-championship/id852396982

###BonApp (November 2015 - February 2016)
Unusual culinary application. I created a new features and bugs fixes. Now in progress
https://itunes.apple.com/app/id984491525

###Spektr (August 2015 - August 2015)
Application to vote through social networks. I had created a mobile application and a server on Parse.com. Provide social-network sharing.
https://itunes.apple.com/by/app/spektr/id1024940689?mt=8 

###BigSmallWedding (July 2015 – August 2015)
Mobile application for wedding people. I created a mobile app, server app on Parse and website for administrator also on Parse.com

###Hamleys (March 2015 - pesent time) 
Application for parents and Hamleys company. Use technology of iBeacons. My role was ios developer.
https://itunes.apple.com/by/developer/hamleys-lubianka-zao/id1020672691

###Lenta.ru (February 2015 – March 2015)
Fast and free application for reading news online newspaper Lenta.ru. For this app I created a Sinatra server with logic for parsing.
https://itunes.apple.com/by/app/novosti-lenta.ru-neoficial/id975805914?mt=8 

###MoonCalendar (December 2014 – December 2014)
Mobile application development. I created a new English version and bugs fixes. 
https://itunes.apple.com/ru/app/lunnyj-kalendar-2015/id948196885?mt=8

###«Здесь не курят. Сообщите о нарушениях антитабачного закона» (Smoke-Free) (September 2014 – present time)
Mobile application development. «Здесь не курят» is an application for help in making a complaint under the anti-smoking law in the Russian Federation.
https://itunes.apple.com/ru/app/zdes-ne-kurat.-soobsite-o/id937272715?mt=8